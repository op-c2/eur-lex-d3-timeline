# EUR-Lex D3-Timeline

## Description
Javascript implementation of a interactive graphical timeline, used to show the evolution of the legal act over time (the changes it has undergone) in EUR-Lex. The timeline allows an easy navigation between the different consolidated versions of the act. The implementation was based on the open-source library D3.js (https://d3js.org/).

The current version of this library is 2.10.6 (https://eur-lex.europa.eu/js/d3-timeline.js?v=2.10.6)

![ALT](images/timeline.PNG "EUR-Lex D3-Timeline")

## Support
For questions about EUR-Lex D3-Timeline, please send us an email to EURLEX-HELPDESK@publications.europa.eu

## Roadmap
The current version of this library is 2.10.6. There are currently no ongoing developments of this concrete implementation.

## Authors and acknowledgment
EUR-Lex Development Provider

## License
Copyright the European Union 2022. Licensed under the EUPL-1.2 or later.

## Project status
Live