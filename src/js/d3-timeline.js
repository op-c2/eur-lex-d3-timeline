/*
Copyright (c) 2021 European Union/


Licensed under the EUPL, Version 1.2 or -- as soon they will be
approved by the European Commission -- subsequent versions of the EUPL
(the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12/


Unless required by applicable law or agreed to in writing, software
distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied. See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//General settings
const d3Timeline = {
  width: $('#d3-timeline').width(),
  height: 180,
  margin: 25,
  yTopLabel: 30,
  yMainAxis: 60,
  yLabelAxis: 110,
  yTimeScalesAxis: 180,
  hasTimeScalesAxis: false,
  color: "#087FAA",
  selectedColor: "#FFD617",
  currentColor: "#74cc0a",
  futurColor: '#84bfd5',
  labelColor: "#337ab7"
};

//svg selector
const d3TimelineWrapper = d3.select("#d3-timeline")
  .append("svg")
  .attr("viewBox", `0 0 ${d3Timeline.width} ${d3Timeline.height}`);

//Method for parsing date string with format YYYY-mm-dd
const parseTime = d3.timeParse("%Y-%m-%d");

//Method for initiating timeline
function initTimeline(data, basicAct) {

  if (data && data.length > 0 && basicAct) {

    //starting date
    const chartStartsAt = data[0]['date'];
    //ending date
    const chartEndsAt = data[data.length - 1]["date"];
    //today
    const today = new Date();

    //list dates
    let dates = [];
    for (let obj of data) {
      dates.push(parseTime(obj.date));
    }

    //apply domain
    const domain = d3.extent(dates);

    //time scale axis
    const xScale = d3.scaleTime()
      .domain(domain)
      .range([d3Timeline.margin, d3Timeline.width - d3Timeline.margin]);

    //time scale axis
    const xAxis = d3.axisBottom(xScale);

    //draw time scale axis conditionally (for testing)
    if(d3Timeline.hasTimeScalesAxis){
        d3TimelineWrapper.append("g")
            .attr("transform", `translate(0,${d3Timeline.yTimeScalesAxis})`)
            .call(xAxis.ticks(d3.timeYear));
    }

    //draw main axis
    drawMainAxis();

    //draw items from provided data
    let items = d3TimelineWrapper.selectAll('g.item')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'item');

    //loop each item and create markup according to their respective type
    items.each(function(item) {
      let elem = d3.select(this);
      const startDate = parseTime(item.date);
      const isAfterToday = item.future;
      const x = xScale(startDate);
      const celex = item.celex;

      //draw period covered by session
      if (item.duration > 0) {
        const endingDate = d3.timeDay.offset(startDate, item.duration);
        drawSelectedLine(elem, x, xScale(endingDate));
      }

      //draw basic act
      if (item.type === 'basic-act') {
        drawBasicAct(elem, x, basicAct);
        addTooltip(elem, basicAct);
      }

      //draw legal act
      if (item.type === 'legal-act') {
        drawMapMarker(elem, {
          x: x,
          y: d3Timeline.yMainAxis,
          r: 15,
          outerColor: item.selected ? d3Timeline.selectedColor : 'white'
        });
        drawMapMarker(elem, {
          x: x,
          y: d3Timeline.yMainAxis,
          hoverEnable: true,
          outerColor: isAfterToday ? d3Timeline.futurColor : d3Timeline.color,
          innerColor: item.current ? d3Timeline.currentColor : 'white',
          celex:celex
        });

        drawLabel(elem, x, item.label);
        addTooltip(elem, celex);
      }

      //draw breakline
      if (item.type === 'breakline') {
        drawBreakline(elem, x - 16);
        drawBreakline(elem, x - 8);
      }

      //draw today
      if(item.type === 'today'){
        drawToday(x, d3Timeline.yMainAxis, item);
      }

    });

  }
}

//Helper method for drawing main axis
function drawMainAxis() {
  d3TimelineWrapper.append("line")
    .style("stroke", d3Timeline.color)
    .style("stroke-width", 12)
    .attr("x1", d3Timeline.margin)
    .attr("y1", d3Timeline.yMainAxis)
    .attr("x2", d3Timeline.width - d3Timeline.margin)
    .attr("y2", d3Timeline.yMainAxis);
}

//Helper method for drawing period line
function drawSelectedLine(element, x1, x2) {

  element.append("line")
    .style("stroke", d3Timeline.selectedColor)
    .style("stroke-width", 4)
    .attr("x1", x1)
    .attr("y1", d3Timeline.yMainAxis)
    .attr("x2", x2)
    .attr("y2", d3Timeline.yMainAxis);
}

//Helper method for drawing basic act
function drawBasicAct(element, x, celex) {

	let groupingElement = element.append("g")
  	    .attr("class","d3-basic-act")
        .on("mouseover", function(d){
    	    d3.select(this).select('.d3-basic-act-rect').attr("fill", 'grey');
        })
        .on("mouseout", function(d){
    	    d3.select(this).select('.d3-basic-act-rect').attr("fill", d3Timeline.color);
        });

    groupingElement.append("rect")
        .attr("class","d3-basic-act-rect")
        .attr("width", 30)
        .attr("height", 30)
        .attr("fill", d3Timeline.color)
        .attr("stroke", 'white')
        .attr("stroke-width", 3)
        .attr("rx", 6)
        .attr("ry", 6)
        .attr('x', x)
        .attr('y', d3Timeline.yMainAxis - 14);

    groupingElement.append("circle")
        .attr("r", 5)
        .attr("fill", 'white')
        .attr("cx", x + 15)
        .attr("cy", d3Timeline.yMainAxis);

    if(celex){
        groupingElement.on('click', e => goToDocumentVersion(e, celex));
    }
}

//Helper method for drawing map marker
function drawMapMarker(element, params) {

  const x = params.x;
  const y = params.y;
  const r = params.r || 12;
  const outerColor = params.outerColor || d3Timeline.color;
  const innerColor = params.innerColor || "#fff";

  let outerElement = element.append("g")
    .attr("class", "d3-map-marker");

  if(params.celex){
  	outerElement.on('click', e => goToDocumentVersion(e, params.celex));
  }

  let circleElement = outerElement.append("circle")
    .attr("class", "d3-map-marker-circle")
    .attr("r", r)
    .attr("fill", outerColor)
    .attr("cx", x)
    .attr("cy", y);

  let path = d3.path();
  path.moveTo(x - r, y + (r / 5));
  path.lineTo(x, y + (r * 2));
  path.lineTo(x + r, y + (r / 5));
  path.closePath();

  let pathElement = outerElement.append("path")
    .attr("class", "d3-map-marker-path")
    .attr("d", path)
    .attr("fill", outerColor);

  let innerCircleElement = outerElement.append("circle")
    .attr("class", "d3-map-marker-inner-circle")
    .attr("r", r / 2.5)
    .attr("fill", innerColor)
    .attr("cx", x)
    .attr("cy", y);

  if (params.hoverEnable) {
    applyHoverOnElement(outerElement, outerColor);
  }

}

//Helper method for applying hover on map marker
function applyHoverOnElement(element, initialColor) {

  element.on("mouseover", function(d) {
    d3.select(this).select('.d3-map-marker-circle').attr("fill", 'grey');
    d3.select(this).select('.d3-map-marker-path').attr("fill", 'grey');
  })
  element.on("mouseout", function(d) {
    d3.select(this).select('.d3-map-marker-circle').attr("fill", initialColor);
    d3.select(this).select('.d3-map-marker-path').attr("fill", initialColor);
  });
}

//Helper method for drawing today markup
function drawToday(x, y, item) {
  d3TimelineWrapper.append("rect")
    .attr("width", 4)
    .attr("height", 24)
    .attr("fill", d3Timeline.color)
    .attr("stroke", "white")
    .attr("stroke-width", 2)
    .attr('x', x)
    .attr('y', y - 14);

  d3TimelineWrapper.append('text')
    .attr('x', x - 14)
    .attr('y', d3Timeline.yTopLabel)
    .attr('stroke', d3Timeline.labelColor)
    .style("font-size", 12)
    .text(item.label);
}

//Helper method for drawing label
function drawLabel(element, x, label) {

  const x1 = x - 45;
  const y1 = d3Timeline.yLabelAxis + 35;

  element.append('text')
    .attr('class', 'd3-timeline-label')
    .attr('stroke', d3Timeline.labelColor)
    .attr("transform", `translate(${x1},${y1}) rotate(320)`)
    .style("font-size", 12)
    .style("font-weight", 300)
    .text(label);
}

//Helper method for accessing document with the provided celex number
function goToDocumentVersion(event, celex) {
  const relativeUrl = $('#d3-timeline').attr('data-relative-url');

  window.open(`${relativeUrl}legal-content/EN/ALL/?uri=CELEX:${celex}`, '_self');
}

//Helper method for drawing breakline
function drawBreakline(element, x) {

  const y = d3Timeline.yMainAxis - 15;

  let path = d3.path();
  path.moveTo(x, y);
  path.lineTo(x + 5, y);
  path.lineTo(x + 24, y + 15);
  path.lineTo(x + 5, y + 30);
  path.lineTo(x, y + 30);
  path.lineTo(x + 20, y + 15);
  path.closePath();

  element.append("path")
    .attr("d", path)
    .attr("fill", 'white');
}

//Helper method for adding tooltip
function addTooltip(element, celex) {
  element.append('title')
    .text(celex);
}